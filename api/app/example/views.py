from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from example.serializers import SomeBodySerializer, SomeQuerySerializer, ExampleEntitySerializer


class ExampleViewSet(GenericViewSet):
    """
    Пример документирования методов.
    """
    serializer_class = ExampleEntitySerializer

    @swagger_auto_schema(request_body=SomeBodySerializer,
                         responses={'200': ExampleEntitySerializer,
                                    '400': 'Some bad request reason',
                                    '404': 'Some not found reason'})
    @action(methods=['post'], detail=False, url_path='post_action_example', url_name='post_action_example')
    def post_action_example(self, *args, **kwargs):
        """Header of the post method.

        Description of the post method.
        """
        return Response()

    @swagger_auto_schema(query_serializer=SomeQuerySerializer,
                         responses={'200': ExampleEntitySerializer,
                                    '400': 'Some bad request reason',
                                    '404': 'Some not found reason'})
    @action(methods=['get'], detail=False, url_path='get_action_example', url_name='get_action_example')
    def get_action_example(self, *args, **kwargs):
        """Header of the get method.

        Description of the get method.
        """
        return Response()
